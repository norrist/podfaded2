import os

import peewee
from podfaded import data_models as db, feed_info

os.environ["PYTEST"] = "True"

test_feed_faster = "http://fasterpodcast.norrist.xyz/faster.xml"
test_feed_gwo = "http://gnuworldorder.info/ogg.atom.xml"


def test_get_feed_info_faster():
    feed_title, feed_description, latest_episode, feed_link = feed_info.get_feed_info(
        test_feed_faster
    )
    assert feed_title == "Faster"
    assert feed_description == ""
    assert latest_episode.year


def test_get_feed_info_gwo():
    feed_title, feed_description, latest_episode, feed_link = feed_info.get_feed_info(
        test_feed_gwo
    )
    assert feed_title == "GNU World Order Linux Cast"
    assert feed_description == ""
    assert latest_episode.year


def test_insert_test_feed():
    try:
        db.Feed.create(feed_url=test_feed_faster)
    except peewee.IntegrityError:
        pass


def test_update_title_faster():
    feed_to_test = db.Feed.get(feed_url=test_feed_faster)
    feed_info.update_title(test_feed_faster)
    # assert feed_to_test.feed_title == "Faster" #TOTDO why does this fail?


def test_get_enclosure_data():
    feed_info.get_enclosure_data(test_feed_faster)


def test_delete_test_feed():
    feed_to_test = db.Feed.get(feed_url=test_feed_faster)
    db.Feed.delete_instance(feed_to_test)
