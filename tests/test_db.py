import os

import peewee
import pytest
from podfaded import data_models as db, feed_info

os.environ["PYTEST"] = "True"

test_feed = "http://gnuworldorder.info/ogg.atm.xml"
test_feed_faster = "http://fasterpodcast.norrist.xyz/faster.xml"


# def test_data_file_exists():
#     data_file = db.data_file_name
#     assert os.path.exists(data_file)


def test_insert_feed():
    db.Feed.create(feed_url="asdf")
    try:
        db.Feed.create(feed_url=test_feed_faster)
    except peewee.IntegrityError:
        pass


def test_insert_duplicate_feed():
    with pytest.raises(peewee.IntegrityError):
        db.Feed.create(feed_url="asdf")


def test_update_title():
    feed_info.update_title(test_feed_faster)


def test_updated_title():
    updated_feed = db.Feed.get(feed_url=test_feed_faster)
    assert updated_feed.feed_title == "Faster"


def test_delete_feed():
    test_record = db.Feed.get(feed_url="asdf")
    test_record.delete_instance()
    test_record = db.Feed.get(feed_url=test_feed_faster)
    test_record.delete_instance()
