from podfaded import app_config


def test_pages_to_scrape():
    assert app_config.pages_to_scrape


def test_feed_urls_to_ignore():
    assert app_config.feeds_to_ignore
