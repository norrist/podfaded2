import os

import pytest
from podfaded import data_models as db, scrape_for_feeds

os.environ["PYTEST"] = "True"


test_text = """
    <p>The Linux Link Tech Show, our  Hosted live, every Wednesday at
    . Check out the TLLTS site for the stream mirror addresses!
    <a href="http://www.thelinuxlink.net/tllts/tllts_ogg.rss"><img
    alt="Podcast rss feed" src="images/rss.png"
    style="border: 0px solid ; width: 28px; height: 16px;" /></a>
    <a href="http://www.thelinuxlink.net/tllts/tllts_ogg.rss">The
    Feed</a><br />
    <a href="http://www.thelinuxlink.net/tllts/tllts.rss">

    <p>From the ashes of the Bad Apples Podcast Klaatu rises with the
    Gnu World Order Ogg and Speex Cast.  He is crazy and he is informed
    and man does he do a fantastic tutorial. Do not miss this cast,
    whatever you do.    </p>
    <a href="http://gnuworldorder.info/ogg.atom.xml"><img
    alt="Podcast rss feed" src="images/rss.png"
    style="border: 0px solid ; width: 28px; height: 16px;" /></a>
    """
test_page_to_scrape = "http://www.thelinuxlink.net"

good_feed_urls = [
    "http://www.thelinuxlink.net/tllts/tllts_ogg.rss",
    "http://gnuworldorder.info/ogg.atom.xml",
]
bad_feed_urls = [
    "http://www.thelinuxink.net/tllts/tllts_ogg.rss",
    "http://gnuworldorder.info/ogg.atm.xml",
]


def test_find_rss():
    for test_feed in good_feed_urls:
        assert test_feed in scrape_for_feeds.find_rss(test_text)


def test_feed_is_valid():
    for test_feed in good_feed_urls:
        assert scrape_for_feeds.feed_is_valid(test_feed) is True


def test_bad_feeds_fail():
    for test_feed in bad_feed_urls:
        assert scrape_for_feeds.feed_is_valid(test_feed) is False


# def test_clean_feed_list():
#     assert (
#         scrape_for_feeds.validate_feed_list(good_feed_urls + bad_feed_urls)
#         == good_feed_urls
#     )


@pytest.mark.slow
def test_get_feeds_from_url():
    for feed in good_feed_urls:
        assert feed in scrape_for_feeds.get_feeds_from_url(test_page_to_scrape)


def test_insert_delete_feeds():
    feeds = good_feed_urls
    scrape_for_feeds.insert_feeds(feeds)
    for feed in feeds:
        test_feed = db.Feed.get(feed_url=feed)
        test_feed.delete_instance()
