import datetime

import pytest
from podfaded import podfaded_status


def days_ago(days):
    return datetime.datetime.now() - datetime.timedelta(days=days)


test_cases = [
    (days_ago(400), 0, 5),
    (days_ago(200), 0, 4),
    (days_ago(200), 15, 4),
    (days_ago(91), 15, 3),
    (days_ago(31), 15, 2),
    (days_ago(15), 7, 0),
]


@pytest.mark.parametrize("last_episode, avg_release_time,expected_status", test_cases)
def test_podfaded_status(last_episode, avg_release_time, expected_status):
    assert podfaded_status.podfaded(last_episode, avg_release_time) == expected_status
