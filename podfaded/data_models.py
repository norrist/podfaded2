import os

from peewee import (
    DateTimeField,
    IntegerField,
    Model,
    MySQLDatabase,
    OperationalError,
    SqliteDatabase,
    TextField,
)

data_file_name = "data.sqlite"


if os.getenv("PYTEST") is not None:
    db = SqliteDatabase(data_file_name)
else:
    db = MySQLDatabase("pod", user="pod")


class BaseModel(Model):
    class Meta:
        database = db


class Feed(BaseModel):
    feed_url = TextField(unique=True)
    feed_title = TextField(default="")
    feed_link = TextField(default="")
    latest_episode = DateTimeField(default="")
    podfaded_status = IntegerField(default="")
    latest_href = TextField(default="")
    latest_title = TextField(default="")


db.connect()
try:
    db.create_tables([Feed])
except OperationalError:
    pass
