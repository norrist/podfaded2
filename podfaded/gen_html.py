import os

from jinja2 import Environment, FileSystemLoader

from app_config import pages_to_scrape
from podfaded import data_models as db

THIS_DIR = os.path.dirname(os.path.abspath(__file__))


def podfaded_bootstrap(podfaded):
    if podfaded > 3:
        return "danger"
    elif podfaded > 1:
        return "warning"
    else:
        return "success"


def gen_page():
    show_data = (
        db.Feed.select()
        .where(db.Feed.feed_title != "")
        .group_by(db.Feed.feed_title)
        .group_by(db.Feed.feed_link)
        .group_by(db.Feed.latest_episode)
        .group_by(db.Feed.latest_title)
        .order_by(db.Feed.latest_episode.desc())
    )

    j2_env = Environment(loader=FileSystemLoader(THIS_DIR), trim_blocks=True)
    outfile = open("podfaded.html", "w+")
    outfile.write(
        j2_env.get_template("template.html").render(
            {"shows": show_data, "sources": pages_to_scrape}
        )
    )


if __name__ == "__main__":
    gen_page()
