import logging
from datetime import datetime
from time import mktime

import feedparser

from podfaded import data_models as db, podfaded_status


def get_feed_info(feed):
    d = feedparser.parse(feed)
    try:
        feed_title = d.feed.title
    except AttributeError:
        feed_title = ""
    try:
        feed_description = d.feed.description
    except AttributeError:
        feed_description = ""
    try:
        feed_link = d.feed.link
    except AttributeError:
        feed_link = ""
    try:
        try:
            latest_episode_struct_time = d.entries[0].updated_parsed
        except AttributeError:
            latest_episode_struct_time = d.updated_parsed
        latest_episode = datetime.fromtimestamp(mktime(latest_episode_struct_time))
    except:  # NOQA
        logging.debug(f"NO Episode date for {feed}")
        latest_episode = datetime.now()  # FIXME
    return (feed_title, feed_description, latest_episode, feed_link)


def get_enclosure_data(feed):
    try:
        d = feedparser.parse(feed)
    except:
        return ("", "")
    try:
        latest_href = d.entries[0].enclosures[0].href
    except (IndexError, AttributeError):
        latest_href = ""
    try:
        latest_title = d.entries[0].title
    except IndexError:
        latest_title = ""
    logging.debug(f"href: {latest_href} episode title: {latest_title}")
    return (latest_href, latest_title)


def update_title(feed):
    try:
        feed_title, feed_description, latest_episode, feed_link = get_feed_info(feed)
    except:
        return
    logging.debug(
        f"feed_title:{feed_title}  latest_episode:{latest_episode}, feed_link:{feed_link} "
    )
    latest_hfef, latest_title = get_enclosure_data(feed)
    feed_to_update = db.Feed.get(feed_url=feed)
    logging.debug(f"updating record {feed_to_update}")
    feed_to_update.feed_title = feed_title
    feed_to_update.feed_link = feed_link
    feed_to_update.latest_episode = latest_episode
    feed_to_update.podfaded_status = podfaded_status.podfaded(
        latest_episode, 1
    )  # FIXME 1
    feed_to_update.latest_href = latest_hfef
    feed_to_update.latest_title = latest_title
    feed_to_update.save()


def insert_status_check(feed):
    feed_to_check = db.Feed.get(feed_url=feed)
    feed_to_check


if __name__ == "__main__":
    for feed in db.Feed.select():
        # if feed.feed_title == "" or feed.feed_link == "":
        update_title(feed.feed_url)
