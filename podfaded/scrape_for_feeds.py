import logging
import socket
import urllib
from urllib.request import urlopen

import feedparser
import peewee
from bs4 import BeautifulSoup

from podfaded import data_models as db
from podfaded.app_config import feeds_to_ignore, pages_to_scrape

socket.setdefaulttimeout(1)

# pages_to_scrape = ["https://wiki.ubuntu.com/Podcasts", "http://www.thelinuxlink.net"]


def find_rss(html_text):  # TODO use bs4 to find links
    found_feeds = []
    for text_segment in html_text.replace('"', " ").split():
        if text_segment.split(".")[-1] in ("rss", "xml"):
            if text_segment.lower().startswith("http"):
                logging.debug(f"found {text_segment}")
                found_feeds.append(text_segment)
    return set(found_feeds)


def find_links(html_text):
    found_links = []
    soup = BeautifulSoup(html_text, "html.parser")
    for link in soup.find_all("a"):
        if link.get("href") not in feeds_to_ignore:
            found_links.append(link.get("href"))
    return found_links


def feed_is_valid(feed_url):
    logging.debug(f"Testing {feed_url}")
    try:  # TODO make this a seperate function to reuse in feed_info
        d = feedparser.parse(feed_url)
        try:
            d.entries[0].updated_parsed
        except AttributeError:
            d.updated_parsed
        logging.debug("Valid")
        return True
    except (
        urllib.error.URLError,
        AttributeError,
        ConnectionResetError,
        socket.timeout,
        IndexError,
    ):
        logging.debug("UrlError")
        return False


def validate_feed_list(feed_list):
    validated_feeds = []
    for feed in feed_list:
        if feed_is_valid(feed):
            validated_feeds.append(feed)
    return validated_feeds


def get_feeds_from_url(page_to_scrape):
    try:
        page_text = str(urlopen(page_to_scrape).read())
    except (urllib.error.URLError, TimeoutError):
        return []
    # feeds = find_rss(page_text)
    feeds = find_links(page_text)
    validated_feeds = validate_feed_list(feeds)
    return validated_feeds


def insert_feeds(feeds):
    for feed_url in feeds:
        try:
            db.Feed.create(feed_url=feed_url)
            logging.debug(f"Adding {feed_url} to DB")
        except peewee.IntegrityError:
            pass


if __name__ == "__main__":
    for page in pages_to_scrape:
        feeds = get_feeds_from_url(page)
        insert_feeds(feeds)
