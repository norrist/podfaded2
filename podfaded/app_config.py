pages_to_scrape = (
    "https://wiki.ubuntu.com/Podcasts",
    "http://www.thelinuxlink.net",
    "http://freeculturepodcasts.org",
)

feeds_to_ignore = (
    "http://shotofjaq.org/feed/",
    "http://feeds.feedburner.com/tbs-mp3",
    "http://www.theregister.co.uk/software/open_season/headlines.rss",
    "http://feeds.feedburner.com/tlf-podcast",
    "http://feeds.feedburner.com/tbs-mp3",
    "http://news.launchpad.net/feed/ ",
)
