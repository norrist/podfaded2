"""
Return int for danger of pod fadding

0 - OK
1 - Slightly off schedule, missed 1 episode, or 15 days since last episode
2 - Off Schedule, missed 2 episodes, or 30 days since last episode
3 - 90 days since last episode
4 - 180 days since last episode
5 - 365 days since last episode


"""

import datetime

Today = datetime.datetime.now()


def podfaded(last_episode, avg_release_time):
    days_since_last_episide = (Today - last_episode).days
    # if avg_release_time == 0:
    #     return 5
    # missed_episodes = days_since_last_episide / avg_release_time

    # if missed_episodes < 2 and days_since_last_episide < 15:
    #     return 0
    # if missed_episodes < 3 and days_since_last_episide < 30:
    #     return 1

    if days_since_last_episide > 364:
        return 5
    elif days_since_last_episide >= 180:
        return 4
    elif days_since_last_episide >= 90:
        return 3
    elif days_since_last_episide >= 30:
        return 2
    elif days_since_last_episide >= 15:
        return 1
    else:
        return 0
