#!/bin/bash
set -euo pipefail
IFS=$'\n\t'
export PYTEST=true

python3 -m venv build-venv
source build-venv/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
pip3 install   -e .
python podfaded/scrape_for_feeds.py
python podfaded/feed_info.py
python podfaded/gen_html.py
